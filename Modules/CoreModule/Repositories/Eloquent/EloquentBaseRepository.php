<?php

namespace Modules\CoreModule\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\CoreModule\Repositories\BaseRepository;

abstract class EloquentBaseRepository implements BaseRepository
{

    public $model;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {

        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function getAll($field='created_at')
    {
        return $this->model->get();
//        return-$this->model->filtered()->sorted()->orderBy($field,'DESC')->get();
    }

    public function paginate($perPage = 20, $field='created_at')
    {
        return $this->model->filtered()->paginate($perPage);
//        return-$this->model->filtered()->sorted()->orderBy($field,'DESC')->paginate($perPage);
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($model, $data)
    {
//        return $model->update($model, $data);
        return $model->update($data);
    }

    public function destroy($model): bool
    {
//        return $model->destroy();
        return $model->delete();
    }

    public function findBySlug($slug)
    {
        return  $this->model->findBySlug($slug);
    }

    public function where(string $field, $value, string $operator = null)
    {
//        dd($field, $value, $operator);
        if ($operator === null) {
            $operator = '=';
        }else{
            list($value, $operator) = [$operator,$value];
        }

        return $this->model->where($field, $operator, $value);
    }

    Public function allWithBuilder(): Builder
    {
        return $this->model->query();
    }

}
