<?php

namespace Modules\CoreModule\Repositories;




interface BaseRepository
{
    /**
     * @param $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function find($id);

    /**
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getAll();

    /**
     * @param $perPage
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 20);

    /**
     * @param $data
     * @return $model
     */
    public function create($data);

    /**
     * @param $model
     * @param array $data
     * @return $model
     */
    public function update($model, $data);

    /**
     * @param $model
     * @return bool
     */
    public function destroy($model):bool;

    /**
     * @param string $slug
     * @return $model
     */
    public function findBySlug($slug);

    /**
     * @param string $field
     * @param $value
     * @param string|null $operator
     * @return mixed
     */
    public function where(string $field, $value, string $operator=null);


}
