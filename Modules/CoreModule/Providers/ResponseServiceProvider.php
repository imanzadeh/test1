<?php

namespace Modules\CoreModule\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    public function register(): void
    {

    }

    public function boot(): void
    {
        Response::macro('success', function ($data, $status=200, $message='با موفقیت اعمال شد'){
            return response()->json([
                'status' => $status,
                'message' => $message,
                'result' => $data,
            ],$status??200 );
        });
        Response::macro('failed', function ($data, $status=400, $message='خطا در اعمال'){
            return response()->json([
                'status' => $status,
                'message' => $message,
                'error' => $data,
            ],$status??400);
        });
    }
}
