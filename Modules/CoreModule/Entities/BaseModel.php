<?php

namespace Modules\CoreModule\Entities;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CoreModule\Traits\Searchable;
use OwenIt\Auditing\Contracts\Auditable;

class BaseModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable,Searchable,SoftDeletes;

    protected $sortable = ['*'];

    public function CreatedTime():attribute
    {
        return Attribute::make(get: fn($value)=> verta($this->created_at)->formatDifference());
    }

    public function updatedTime():attribute
    {
        return Attribute::make(get: fn($value)=> verta($this->updated_at)->formatDifference());
    }

    public function deletedTime():attribute
    {
        return Attribute::make(get: fn($value)=> verta($this->deleted_at)->formatDifference());
    }
}
