<?php

namespace Modules\CoreModule\Traits;


use InvalidArgumentException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Request;

trait Searchable
{
    /**
     * @param Builder $builder
     * @param array $query
     * @return void
     */
    public function scopeFiltered(Builder $builder, array $query=[])
    {
        dump('test');
//        $query=$query?:Request::all();
//
//        $this->validateFieldName($query);
    }

    public function validateFieldName(array $query)
    {
        foreach ($query as $fieldName => $value){

            if (!preg_match('/^[a-zA-Z_\$][a-zA-Z0-9_\$\.]{0,63}$/', $fieldName)){
                throw new InvalidArgumentException('Incorrect field name {$fieldName}');
            }
        }
    }


}
