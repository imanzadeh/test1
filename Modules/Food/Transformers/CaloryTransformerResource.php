<?php

namespace Modules\Food\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CaloryTransformerResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'value' => $this->value,
            'created_at' => verta($this->created_at)->formatDifference(),
            'updated_at' => verta($this->updated_at)->formatDifference()
        ];
    }
}
