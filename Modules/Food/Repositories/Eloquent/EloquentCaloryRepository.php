<?php

namespace Modules\Food\Repositories\Eloquent;

use Modules\CoreModule\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Food\Entities\Calory;
use Modules\Food\Repositories\CaloryRepository;
use Modules\Food\Repositories\FoodRepository;

class EloquentCaloryRepository extends EloquentBaseRepository implements CaloryRepository
{

    public function paginate($perPage = 20, $field = 'created_at')
    {
        return $this->model->filtered()->paginate($perPage);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($data)
    {
        $result = $this->model->create($data);
        return $result;
    }
    public function getAll($field = 'created_at')
    {
        return $this->model->get();
    }

    public function update($model, $data)
    {
//        dd($model,$data);
//        $result=$this->model->update($model, $data);
        $result=$model->update($data);
        return $result;
    }

    public function destroy($model): bool
    {
        return $model->delete();
    }


    public function where(string $field, $value, string $operator = null)
    {

        return $this->model->where($field,$operator,$value)->get();
    }

}
