<?php

namespace Modules\Food\Repositories\Eloquent;

use Modules\CoreModule\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Food\Repositories\FoodRepository;

class EloquentFoodRepository extends EloquentBaseRepository implements FoodRepository
{

    public function paginate($perPage = 20, $field = 'created_at')
    {
        return $this->model->filtered()->sorted()-orderBy($field,'DESC')->paginate($perPage);
    }

}
