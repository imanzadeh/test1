<?php

namespace Modules\Food\Entities;

use Modules\CoreModule\Entities\BaseModel;

class Calory extends BaseModel
{

    protected $fillable = ['title','value'];
    protected $searchable = ['id','title','value'];
    protected $sortable = ['*'];




}
