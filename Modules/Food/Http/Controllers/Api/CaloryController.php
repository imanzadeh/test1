<?php

namespace Modules\Food\Http\Controllers\Api;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\CoreModule\Http\Controllers\ApiController;
use Modules\Food\Entities\Calory;
use Modules\Food\Repositories\CaloryRepository;
use Modules\Food\Transformers\CaloryTransformerResource;

class CaloryController extends ApiController
{
    protected $repository;


    public function __construct(CaloryRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $calories = $this->repository->filtered()->paginate($request->perPage ?? 20);
        return response()->success(['items' => CaloryTransformerResource::collection($calories)], 200, 'test');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try {


//        dd($request->all());
            $result = $this->repository->create($request->all());
            return response()->success(['item' => CaloryTransformerResource::collection($result)], 201, 'Created successfully');
        }catch (\Throwable $e) {
            return response()->failed([], $e->getCode(), $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param Calory $calory
     * @return Renderable
     */
    public function show(Calory $calory)
    {
        $result = $this->repository->find($calory);
        return response()->success(['item' => CaloryTransformerResource::collection($result)], 200, 'Found successfully');
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Calory $calory
     * @return Renderable
     */
    public function update(Request $request, Calory $calory)
    {
        $result=$this->repository->update($calory,$request->all());
        return response()->success(['item' => CaloryTransformerResource::collection($result)], 200, 'Updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     * @param Calory $calory
     * @return Renderable
     */
    public function destroy(Calory $calory)
    {
        $this->repository->destroy($calory);
        return response()->success([], 200, 'Deleted successfully');
    }


    public function filter(Request $request)
    {
        $filters = utf8_encode($request->input('filter'));
        $filtersArray = json_decode($filters, true);
//        dd($filtersArray);

//        foreach ($filtersArray as $filter) {
            $field = $filtersArray['field'];
            $operation =$filtersArray['operation'];
            $value = $filtersArray['value'];

//        }

        $result= $this->repository->where($field,$value,$operation);
        return response()->success(['item' => CaloryTransformerResource::collection($result)], 200, 'Fetched successfully');
    }
}
