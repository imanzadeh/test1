<?php

namespace Modules\Food\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Food\Entities\Calory;
use Modules\Food\Repositories\CaloryRepository;
use Modules\Food\Repositories\Eloquent\EloquentCaloryRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CaloryRepository::class, function () {
            return new EloquentCaloryRepository(new Calory());
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
